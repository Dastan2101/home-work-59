import React, {Component} from 'react';
import './App.css';
import '../components/AddMovie/AddMovieForm'
import AddMovieForm from "../components/AddMovie/AddMovieForm";
import Movie from "../components/Movie/Movie";


class App extends Component {

    state = {
        value: '',
        movies: [],
    };

    changeHandler = (event) => {
        this.setState({
            value: event.target.value
        });
    };

    changeMovie = (value, index) => {

        let moviesArray = this.state.movies;

        moviesArray[index] = value;

        this.setState({movies: moviesArray});

    };


    AddMovie = () => {

        let value = this.state.value;

        if (value !== '') {
            let moviesArray = this.state.movies;

            moviesArray.push(value);

            this.setState({movies: moviesArray});
        } else {
            alert('Enter some text')
        }

    };

    DeleteMovie = (index) => {

        let moviesArray = this.state.movies;

        moviesArray.splice(index, 1);

        this.setState({movies: moviesArray})

    };

    render() {
        return (
            <div className="App">
                <AddMovieForm changeHandler={(event) => this.changeHandler(event)} AddMovie={() => this.AddMovie()}
                              value={this.state.value}/>
                <div className="watch-list-block">
                    <h2>To watch list: </h2>
                    {this.state.movies.map((movie, id) =>
                        <Movie
                            key={id}
                            value={movie}
                            changed={(e) => this.changeMovie(e.target.value, id)}
                            deleteIndex={() => this.DeleteMovie(id)}
                        />)}

                </div>
            </div>
        );
    }
}

export default App;
