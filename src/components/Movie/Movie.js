import React, {Component} from 'react';
import './MovieStyles.css';

class Movie extends Component {

    shouldComponentUpdate(nextProps) {
        return this.props.value !== nextProps.value;
    }

    render() {
        return (
            <div className="movie-block">
                <input type="text" className="movie-input" value={this.props.value} onChange={this.props.changed}/>
                <button className="movie-btn" onClick={this.props.deleteIndex}>x</button>
            </div>
        )
    }

}

export default Movie;