import React from 'react';
import './AddMovieFormStyles.css';

const AddMovieForm = (props) => {
    return (
        <div style={{marginTop: '50px', padding: '30px'}}>
            <input type="text" className="add-form-input" onChange={props.changeHandler} value={props.value}/>
            <button className="add-form-btn" type="button" onClick={props.AddMovie}>Add</button>
        </div>
    );
};

export default AddMovieForm;